package az.loans;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoanMsController {

    public static void main(String[] args) {
        SpringApplication.run(LoanMsController.class, args);
    }
}
