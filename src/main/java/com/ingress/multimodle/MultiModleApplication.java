package com.ingress.multimodle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiModleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiModleApplication.class, args);
    }

}
